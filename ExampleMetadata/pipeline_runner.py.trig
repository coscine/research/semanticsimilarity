@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix ns1: <https://purl.org/coscine/ontologies/factgraph/> .
@prefix ns2: <http://dkm.fbk.eu/ontologies/knowledgestore#> .
@prefix ns3: <http://framebase.org/ns/> .
@prefix ns4: <http://semanticweb.cs.vu.nl/2009/11/sem/> .
@prefix ns5: <https://purl.org/coscine/resources/pipeline_runner.py/@type=data&version=1&extracted=> .
@prefix tika: <https://purl.org/coscine/ontologies/tika/> .

ns1:pipeline_runner.py {
    <entity:electronmicroscope.jpg> ns2:mod <http://pikes.fbk.eu/#electronmicroscope.jpg>,
            <http://pikes.fbk.eu/#exampletextimage.jpg>,
            <http://pikes.fbk.eu/#todosaus191212.txt> .

    <entity:testtextchangedcopy.txt> ns2:mod <http://pikes.fbk.eu/##fileinfos> .

    <http://pikes.fbk.eu/#%5c> ns2:mod <attr:%5c> .

    <http://pikes.fbk.eu/#PXY> ns2:mod <entity:persistent_identifiers>,
            <http://pikes.fbk.eu/#refs/20> .

    <http://pikes.fbk.eu/#announce> a <http://www.newsreader-project.eu/ontologies/framenet/Statement>,
            <http://www.newsreader-project.eu/ontologies/propbank/announce.01>,
            <http://www.newsreader-project.eu/ontologies/verbnet/reflexive_appearance-48.1.2> ;
        ns3:fe-statement-message <entity:amazon_textract> ;
        ns3:fe-statement-speaker <http://www.newsreader-project.eu/time/2018> ;
        ns4:hasActor <entity:amazon_textract> .

    <http://pikes.fbk.eu/#be> a <http://www.newsreader-project.eu/ontologies/propbank/be.01> ;
        ns4:hasActor <entity:systems>,
            <http://pikes.fbk.eu/#here>,
            <http://pikes.fbk.eu/#stay.pdf> .

    <http://pikes.fbk.eu/#change> a <http://www.newsreader-project.eu/ontologies/framenet/Undergo_change>,
            <http://www.newsreader-project.eu/ontologies/propbank/change.01>,
            <http://www.newsreader-project.eu/ontologies/verbnet/other_cos-45.4> ;
        ns4:hasActor <entity:title> .

    <http://pikes.fbk.eu/#coscine> ns2:mod <http://pikes.fbk.eu/#c> .

    <http://pikes.fbk.eu/#element> a <http://www.newsreader-project.eu/ontologies/framenet/Ingredients>,
            <http://www.newsreader-project.eu/ontologies/nombank/element.01> ;
        ns2:mod <http://pikes.fbk.eu/#figure> ;
        ns4:hasActor <http://pikes.fbk.eu/#figure> .

    <http://pikes.fbk.eu/#example> a <http://www.newsreader-project.eu/ontologies/framenet/Instance> .

    <http://pikes.fbk.eu/#extract> a <http://www.newsreader-project.eu/ontologies/framenet/Removing>,
            <http://www.newsreader-project.eu/ontologies/propbank/extract.01>,
            <http://www.newsreader-project.eu/ontologies/verbnet/remove-10.1> ;
        ns3:fe-removing-theme <http://pikes.fbk.eu/#text> ;
        ns4:hasActor <entity:tika_behavior>,
            <http://pikes.fbk.eu/#text> .

    <http://pikes.fbk.eu/#file> a <http://www.newsreader-project.eu/ontologies/framenet/Placing> .

    <http://pikes.fbk.eu/#fruits.jpeg> ns2:mod <http://pikes.fbk.eu/#neondstowertemperaturedata.hdf5>,
            <http://pikes.fbk.eu/#run18032020124307.h5> .

    <http://pikes.fbk.eu/#have> a <http://www.newsreader-project.eu/ontologies/framenet/Possession>,
            <http://www.newsreader-project.eu/ontologies/propbank/have.03>,
            <http://www.newsreader-project.eu/ontologies/verbnet/own-100> .

    <http://pikes.fbk.eu/#identifier> ns2:mod <http://pikes.fbk.eu/#fileinfos> .

    <http://pikes.fbk.eu/#information> a <http://www.newsreader-project.eu/ontologies/framenet/Information>,
            <http://www.newsreader-project.eu/ontologies/nombank/information.01>,
            <http://www.newsreader-project.eu/ontologies/propbank/inform.01>,
            <http://www.newsreader-project.eu/ontologies/verbnet/tell-37.2> ;
        ns2:mod <attr:not>,
            <entity:tika_behavior>,
            <http://pikes.fbk.eu/#meta>,
            <http://pikes.fbk.eu/#only> ;
        ns4:hasActor <entity:tika_behavior>,
            <http://pikes.fbk.eu/#meta> .

    <http://pikes.fbk.eu/#join> a <http://www.newsreader-project.eu/ontologies/framenet/Becoming_a_member>,
            <http://www.newsreader-project.eu/ontologies/propbank/join.01> ;
        ns4:hasActor <http://pikes.fbk.eu/#testtextcopy.txt>,
            <http://pikes.fbk.eu/#uuid> .

    <http://pikes.fbk.eu/#listdir> ns2:mod <http://pikes.fbk.eu/#isfile> .

    <http://pikes.fbk.eu/#main> a <http://www.newsreader-project.eu/ontologies/framenet/Importance> .

    <http://pikes.fbk.eu/#name> a <http://www.newsreader-project.eu/ontologies/framenet/Being_named> .

    <http://pikes.fbk.eu/#os> ns2:mod <http://pikes.fbk.eu/#import> .

    <http://pikes.fbk.eu/#os.path.dirname> ns2:mod <attr:=> .

    <http://pikes.fbk.eu/#platform> ns2:mod <http://pikes.fbk.eu/#integration> .

    <http://pikes.fbk.eu/#str> ns2:mod <attr:=>,
            <http://pikes.fbk.eu/#fixeduuid> .

    <http://pikes.fbk.eu/#describe> a <http://www.newsreader-project.eu/ontologies/framenet/Communicate_categorization>,
            <http://www.newsreader-project.eu/ontologies/propbank/describe.01> ;
        ns4:hasActor <http://pikes.fbk.eu/#it> .

    <http://pikes.fbk.eu/#fileinfos> ns2:mod <attr:=> .

    <http://pikes.fbk.eu/#here> a <http://www.newsreader-project.eu/ontologies/framenet/Locative_relation> .

    <http://pikes.fbk.eu/#import> a <http://www.newsreader-project.eu/ontologies/framenet/Import_export> .

    <http://pikes.fbk.eu/#integration> a <http://www.newsreader-project.eu/ontologies/nombank/integration.01> .

    <http://pikes.fbk.eu/#isfile> ns2:mod <attr:os.path> .

    <http://pikes.fbk.eu/#only> a <http://www.newsreader-project.eu/ontologies/framenet/Sole_instance> .

    <entity:amazon_textract> a <http://www.newsreader-project.eu/ontologies/PERSON> .

    <http://pikes.fbk.eu/#text> a <http://www.newsreader-project.eu/ontologies/nombank/text.01> .

    <entity:tika_behavior> ns2:mod <http://pikes.fbk.eu/#describe> .
}

ns5:true {
    <https://purl.org/coscine/resources/pipeline_runner.py> a dcat:Catalog,
            dcat:Distribution ;
        dcterms:created "2022-11-10 18:40:22.277825" ;
        dcterms:format "text/plain; charset=UTF-8" ;
        dcterms:identifier "pipeline_runner.py" ;
        dcterms:modified "2022-09-21 21:44:27.606005" ;
        dcat:byteSize "5330" ;
        dcat:mediaType "text/plain; charset=utf-8" ;
        tika:Content_Encoding "UTF-8" .
}

