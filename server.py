from flask import Flask, request, jsonify
from flask.json import JSONEncoder
from flask_restx import Api, Resource, fields
import os
import importlib

import numpy as np

from rdflib.graph import ConjunctiveGraph

from SemanticSimilarity.SemanticSimilarity import SemanticSimilarity
from SemanticSimilarity import __version__


app = Flask(__name__)
api = Api(app, version=__version__, title='Semantic Similarity API',
    description='This API computes the simlarity of RDF entities',
)

class NumpyEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)
app.json_encoder = NumpyEncoder

def parseGraphs(key: str, data: dict):
    graphs = []
    if key in data:
        for applicationProfileObject in data[key]:
            rdfFormat = "turtle"
            if "type" in applicationProfileObject:
                rdfFormat = applicationProfileObject["type"]
            g = ConjunctiveGraph()
            g.parse(data=str(applicationProfileObject["definition"]), format=rdfFormat)
            graphs.append(g)
    return graphs

# Dynamic import based on config
def importDependencies(methods):
    packageName = "SemanticSimilarity.Similarities"
    methodsList = []
    for val in methods:
        module = importlib.import_module(
            packageName + "." + val
        )
        globals().update({val: module})
        instance = eval(val + "." + val + "()")
        methodsList.append(instance)
    return methodsList

definitionObject = api.model('DefinitionObject', { "definition": fields.String, "type": fields.String })

similarityInput = api.model('SimilarityInput', {
    "graphs": fields.List(fields.Nested(definitionObject)),
    "methods": fields.List(fields.String),
})

similarityMatrix = api.model('SimilarityMatrix', {
    "graphDistances": fields.List(fields.List(fields.Float)),
    "fileDistances": fields.List(fields.List(fields.Float)),
})
similarityReturnOutput = api.model('SimilarityReturnOutput', {
    "similarity_matrix": fields.Nested(similarityMatrix),
    "time_spent": fields.Float,
})
similarityOutput = api.model('SimilarityOutput', {
    "method": fields.String,
    "returnObject": fields.Nested(similarityReturnOutput),
})

@api.route("/")
class SemanticSimilarityWorker(Resource):
    '''Performs the Semantic Similarity'''
    @api.expect(similarityInput)
    @api.response(200, 'Success', [similarityOutput])
    def post(self):
        data = request.json

        graphs = parseGraphs("graphs", data)
        if "methods" in data:
            methods = importDependencies(data["methods"])
        else:            
            methods = importDependencies(["FilterStructureSimplerJaccardSimilarity"])

        semanticSimilarity = SemanticSimilarity(methods)

        return jsonify(semanticSimilarity.getSimilarities(graphs))


versionModel = api.model('Version', {
    "version": fields.String()
})
@api.route("/version")
class VersionWorker(Resource):
    '''Returns the current version of the Semantic Similarity'''
    @api.response(200, 'Success', versionModel)
    def get(self):
        return jsonify({"version": __version__})


if __name__ == "__main__":
    from waitress import serve
    serve(
        app,
        host=os.environ.get("SEMANTICSIMILARITYHOST", "0.0.0.0"),
        port=os.environ.get("SEMANTICSIMILARITYPORT", 36543),
    )
