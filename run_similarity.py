import os
import numpy as np
import pandas
import glob
from rdflib.graph import ConjunctiveGraph
import matplotlib.pyplot as plt
import seaborn as sns
from pathlib import Path
import json
from sklearn.cluster import DBSCAN

from defaultConfigs import setDefaultLogging

setDefaultLogging()

from SemanticSimilarity.GraphParser.NoiseGraphTransformParserOnlyDescriptive import NoiseGraphTransformParserOnlyDescriptive
from SemanticSimilarity.GraphParser.NoiseGraphTransformParserOnlyTechnical import NoiseGraphTransformParserOnlyTechnical

from SemanticSimilarity.Similarities.GraphJaccardSimilarity import GraphJaccardSimilarity
from SemanticSimilarity.Similarities.FilterSimilarity import FilterSimilarity
from SemanticSimilarity.Similarities.FilterStructureSimilarity import FilterStructureSimilarity
from SemanticSimilarity.Similarities.FilterStructureMemorySimilarity import FilterStructureMemorySimilarity
from SemanticSimilarity.Similarities.FilterStructureSimplerSimilarity import FilterStructureSimplerSimilarity
from SemanticSimilarity.Similarities.FilterStructureSimplerJaccardSimilarity import FilterStructureSimplerJaccardSimilarity
from SemanticSimilarity.Similarities.FilterStructureSimplerCosineSimilarity import FilterStructureSimplerCosineSimilarity
from SemanticSimilarity.Similarities.FilterAdaptiveSimilarity import FilterAdaptiveSimilarity
from SemanticSimilarity.Similarities.FilterFullTripleSimilarity import FilterFullTripleSimilarity
from SemanticSimilarity.Similarities.FilterSubSetSimilarity import FilterSubSetSimilarity
from SemanticSimilarity.Similarities.SPARQLSimplifiedConnectorSimilarity import SPARQLSimplifiedConnectorSimilarity
from SemanticSimilarity.Similarities.SPARQLConnectorSimilarity import SPARQLConnectorSimilarity
from SemanticSimilarity.Similarities.SPARQLSimplifiedSimilarity import SPARQLSimplifiedSimilarity
from SemanticSimilarity.Similarities.SPARQLSimilarity import SPARQLSemanticSimilarity
from SemanticSimilarity.Similarities.NetworkxSimilarity import (
    GraphEditDistanceSimilarity,
    OptimizeGraphEditDistanceSimilarity,
    SimRankSimilarity
)
from SemanticSimilarity.Similarities.CHISimilarity import (
    CHIHashSimilarity,
    CHIModifiedSimilarity,
    CHISizeSimilarity
)
from SemanticSimilarity.Similarities.CosineBinarySimilarity import CosineBinarySimilarity
from SemanticSimilarity.Similarities.CustomPreprocessSemanticSimilarity import (
    CustomPreprocessSemanticSimilarity,
)
from SemanticSimilarity.Similarities.CustomSemanticSimilarity import (
    CustomSemanticSimilarity,
)
from SemanticSimilarity.Similarities.JaccardBinarySimilarity import (
    JaccardBinarySimilarity,
)
from SemanticSimilarity.SemanticSimilarity import SemanticSimilarity

if __name__ == '__main__':

    out_dir = "./distance_out"

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    trigfiles: "list[str]" = []
    for trigfile in glob.glob("./ExampleMetadata/*.trig"):
        trigfiles.append(trigfile)

    graphs: "list[ConjunctiveGraph]" = []
    for trigfile in trigfiles:
        graph = ConjunctiveGraph()
        graph.parse(trigfile, format="trig")
        graphs.append(graph)

    files = glob.glob("./ExampleDataset/*.*")

    semanticSimilarity = SemanticSimilarity(
        [
            GraphJaccardSimilarity(),
            FilterSimilarity(),
            #FilterStructureSimilarity(),
            #FilterStructureMemorySimilarity(),
            FilterStructureSimplerSimilarity(),
            FilterStructureSimplerJaccardSimilarity(),
            FilterStructureSimplerCosineSimilarity(),
            #FilterStructureSimplerSimilarity(graphparser=NoiseGraphTransformParserOnlyDescriptive()),
            #FilterStructureSimplerSimilarity(graphparser=NoiseGraphTransformParserOnlyTechnical()),
            #FilterAdaptiveSimilarity(),
            #FilterFullTripleSimilarity(),
            #FilterSubSetSimilarity(),
            #SPARQLSimplifiedConnectorSimilarity(),
            #SPARQLConnectorSimilarity(),
            #SPARQLSimplifiedSimilarity(),
            #SPARQLSemanticSimilarity(),
            #SimRankSimilarity(),
            #OptimizeGraphEditDistanceSimilarity(),
            #GraphEditDistanceSimilarity(),
            #CHIHashSimilarity(),
            #CHIModifiedSimilarity(),
            #CHISizeSimilarity(),
            CosineBinarySimilarity(),
            JaccardBinarySimilarity(),
            #CustomPreprocessSemanticSimilarity(),
            #CustomSemanticSimilarity(),
        ]
    )

    distances = semanticSimilarity.getSimilarities(graphs, files)

    print(distances)

    def render(distance, entry, time_spent, labels, title, out_dir):
        if np.average(entry) != 1:
            df = pandas.DataFrame(entry, columns=labels, index=labels)

            df.to_csv(out_dir + "/" + distance + title + "Similarity.csv")
            #df.apply(lambda s: s.abs().nlargest(3).index.tolist(), axis=1).to_json(out_dir + "/" + distance + title + "SimilarityClosest.json")
            plt.autoscale()
            sns.heatmap(df, annot=len(labels) < 25)
            plt.tight_layout()
            fig = plt.gcf()
            fig.set_size_inches(18.5, 10.5, forward=True)
            plt.savefig(out_dir + "/" + distance + title + "heatmap.pdf", dpi=300)
            plt.title(distance)
            plt.show()
            plt.clf()

            with open(out_dir + "/" + distance + title + "time_spent.txt", "w") as f:
                f.write(str(time_spent))

    graphLabels = [Path(entry).stem[-30:] for entry in trigfiles]
    fileLabels = [Path(entry).stem[-30:] for entry in files]

    for distance in distances:
        distanceMesaure = distance["method"]
        returnObject = distance["returnObject"]
        clusters = DBSCAN(min_samples=1).fit_predict(returnObject["similarity_matrix"]["graphDistances"])
        print(clusters)
        relation = {}
        count = 0
        for clusterEntry in clusters:
            if not (str(clusterEntry) in relation):
                relation[str(clusterEntry)] = []
            relation[str(clusterEntry)].append(trigfiles[count])
            count += 1
        print(relation)
        with open(out_dir + "/" + distanceMesaure + "clusters" + ".json", "w") as f:
            f.write(json.dumps(relation, indent=4, sort_keys=True))
        render(distanceMesaure, returnObject["similarity_matrix"]["graphDistances"], returnObject["time_spent"], graphLabels, "graph", out_dir)
        render(distanceMesaure, returnObject["similarity_matrix"]["fileDistances"], returnObject["time_spent"], fileLabels, "file", out_dir)
