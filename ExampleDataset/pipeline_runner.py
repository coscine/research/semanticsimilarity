import os, os.path

from defaultConfigs import setDefaultLogging, getDefaultConfig

setDefaultLogging()

from MetadataExtractor.pipeline import run_pipeline

current_dir = os.path.dirname(os.path.realpath(__file__))
examples_folder = current_dir + "\\Examples\\"
code_folder = current_dir + "\\MetadataExtractor\\"

fileInfos = [
    {"identifier": None, "file": examples_folder + "TestText.txt"},
    {"identifier": None, "file": examples_folder + "TestTextCopy.txt"},
    {"identifier": None, "file": examples_folder + "TestTextChangedCopy.txt"},
    {"identifier": None, "file": "https://www.w3.org/TR/2014/REC-json-ld-20140116/"},
    {"identifier": None, "file": "https://www.w3.org/TR/json-ld11/"},
    {"identifier": None, "file": "./Papers/DoctoralConsortium/DoctoralConsortium.pdf"},
    # Title changed
    {"identifier": None, "file": "./tmp/DummyDoctoralConsortium.pdf"},
    {"identifier": None, "file": "./Research Proposal/Proposal.pdf"},
    {
        "identifier": None,
        "file": "./Refs/20 Years of Persistent Identifiers – Which Systems are Here to Stay.pdf",
    },
    {
        "identifier": None,
        "file": "./Refs/Pérez2018_Article_ASystematicReviewOfProvenanceS.pdf",
    },
    {"identifier": None, "file": "./Refs/07280815.pdf"},
    {"identifier": None, "file": "./Refs/2_iis_2015_81-90.pdf"},
    {"identifier": None, "file": "./Refs/mepdaw_paper_2.pdf"},
    {"identifier": None, "file": "./Refs/document.pdf"},
    {"identifier": None, "file": "./Refs/707.pdf"},
    {"identifier": None, "file": "./Refs/1-s2.0-S0167739X17305344-main.pdf"},
    {"identifier": None, "file": "./Old/1-s2.0-0025556477901420-main.pdf"},
    {
        "identifier": None,
        "file": examples_folder
        + "AWS re_Invent 2018 – Announcing Amazon Textract (1080p_30fps_H264-128kbit_AAC).mp4",
    },
]

fileInfos = [
    {"identifier": "TestText.txt", "file": examples_folder + "TestText.txt"},
    {"identifier": "TestTextChangedCopy.txt", "file": examples_folder + "TestTextChangedCopy.txt"},
    {"identifier": "TestTextCompleteCopy.txt", "file": examples_folder + "TestTextCompleteCopy.txt"},
    {"identifier": "TestTextCopyDifferentTimestamp.txt", "file": examples_folder + "TestTextCopyDifferentTimestamp.txt"},
    # Tika Behavior: Meta Information and the text extracted
    {"identifier": "Paper.pdf", "file": examples_folder + "Paper.pdf"},
    # Tika Behavior: Meta Information and the text extracted
    # { 'identifier': None, 'file': ".\\Integration Platform – Future Visions.pptx" },
    # Tika Behavior: Meta Information and the text from the figure elements
    # { 'identifier': None, 'file': ".\\CoscineResearchDataCompanion.xlsx" },
    # Tika Behavior: Only Meta information, not text
    # { 'identifier': None, 'file': ".\\FDM Workshop Brainstorming.jpg" },
    # Tika Behavior: It extracts the content of the zip and describes the zip as meta information
    # "content" has the combined content!
    # { 'identifier': None, 'file': ".\\Old\\minimal_example.zip" },
    # { 'identifier': None, 'file': ".\\TodosAus191212.txt" },
    {
        "identifier": "ExampleTextImage.jpg",
        "file": examples_folder + "ExampleTextImage.jpg",
    },
    {
        "identifier": "ElectronMicroscope.jpg",
        "file": examples_folder + "ElectronMicroscope.jpg",
    },
    {"identifier": "TicketTest.jpg", "file": examples_folder + "TicketTest.jpg"},
    {"identifier": "harvard.wav", "file": examples_folder + "harvard.wav"},
    {
        "identifier": "AWS re_Invent 2018 – Announcing Amazon Textract (1080p_30fps_H264-128kbit_AAC).mp4",
        "file": examples_folder
        + "AWS re_Invent 2018 – Announcing Amazon Textract (1080p_30fps_H264-128kbit_AAC).mp4",
    },
    {"identifier": "fruits.jpeg", "file": examples_folder + "fruits.jpeg"},
    # { 'identifier': None, 'file': "C:\\coscine\\logs\\filelogger.log" },
    {"identifier": "pipeline.py", "file": code_folder + "pipeline.py"},
    {
        "identifier": "PERSP-19-00079heilmann_Supp2.mp4",
        "file": examples_folder + "PERSP-19-00079heilmann_Supp2.mp4",
    },
    {
        "identifier": "NEONDSTowerTemperatureData.hdf5",
        "file": examples_folder + "NEONDSTowerTemperatureData.hdf5",
    },
    {
        "identifier": "Run18032020124307.h5",
        "file": examples_folder + "Run18032020124307.h5",
    },
    {
        "identifier": "Run18032020124307BigModified.h5",
        "file": examples_folder + "Run18032020124307BigModified.h5",
    },
    {
        "identifier": "Run18032020124307SmallModified.h5",
        "file": examples_folder + "Run18032020124307SmallModified.h5",
    },
]

#fileInfos = [
#    {"identifier": "TestTextChangedCopy.txt", "file": examples_folder + "TestTextChangedCopy.txt"},
#    {"identifier": "TestTextCopy.txt", "file": examples_folder + "TestTextCopy.txt"},
#]
from os import listdir
from os.path import isfile, join
import uuid

fixedUUID = str(uuid.uuid4())

mypath = examples_folder
fileInfos = [{ 'identifier': fixedUUID + "/" + f, 'file': mypath + f, 'version': "1234" } for f in listdir(mypath) if isfile(join(mypath, f))]

config = getDefaultConfig()

if __name__ == "__main__":
    run_pipeline(fileInfos, config)
