from abc import ABCMeta, abstractmethod
import numpy as np
from rdflib import ConjunctiveGraph
from SemanticSimilarity.Iterators.IIterator import IIterator
from SemanticSimilarity.Iterators.SemiIterator import SemiIterator
from SemanticSimilarity.Preprocessors.IPreprocessor import IPreprocessor
from SemanticSimilarity.Preprocessors.EmptyPreprocessor import EmptyPreprocessor
from SemanticSimilarity.FileParser.IFileParser import IFileParser
from SemanticSimilarity.FileParser.EmptyFileParser import EmptyFileParser
from SemanticSimilarity.GraphParser.IGraphParser import IGraphParser
from SemanticSimilarity.GraphParser.EmptyGraphParser import EmptyGraphParser

import logging

log = logging.getLogger(__name__)


class ISemanticSimilarity:
    __metaclass__ = ABCMeta

    def __init__(
        self,
        preprocessor: "IPreprocessor" = EmptyPreprocessor(),
        fileparser: "IFileParser" = EmptyFileParser(),
        graphparser: "IGraphParser" = EmptyGraphParser(),
        iterator: "IIterator" = SemiIterator(),
    ):
        self.__preprocessor = preprocessor
        self.__fileparser = fileparser
        self.__graphparser = graphparser
        self.__iterator = iterator

    def preprocess(self, graphs: "list[ConjunctiveGraph]", files: "list[str]"):
        return self.__preprocessor.preprocess(graphs, files)

    @abstractmethod
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph") -> float:
        raise NotImplementedError

    @abstractmethod
    def calculateFileSimilarity(self, file1: "str", file2: "str") -> float:
        raise NotImplementedError

    def calculateSimilarityMatrix(self, graphs: "list[ConjunctiveGraph]", files: "list[str]"):
        """
        This method calculates the similarity matrix for the graphs and files.
        It is expected that two graphs or files g1 and g2 behave with the respective implemented calculating methods, so that:
        calculate(g1, g2) == calculate(g2, g1)
        """
        graphDistances = np.identity(len(graphs), dtype=float)
        parsedGraphs = [self.__graphparser.parseGraph(graphs) for graphs in graphs]
        for i, j in self.__iterator.generateIterator(graphs):
            log.debug(
                "Working on graph ("
                + str(i + 1)
                + "/"
                + str(len(graphs))
                + ") and graph ("
                + str(j + 1)
                + "/"
                + str(len(graphs))
                + ")!"
            )
            distance = self.calculateGraphSimilarity(parsedGraphs[i], parsedGraphs[j])
            self.__iterator.handleDistanceSetting(graphDistances, distance, i, j)

        fileDistances = np.identity(len(files), dtype=float)
        parsedFiles = [self.__fileparser.parseFile(file) for file in files]
        for i, j in self.__iterator.generateIterator(files):
            log.debug(
                "Working on "
                + files[i]
                + " ("
                + str(i + 1)
                + "/"
                + str(len(files))
                + ") and "
                + files[j]
                + " ("
                + str(j + 1)
                + "/"
                + str(len(files))
                + ")!"
            )
            measuringFile1, measuringFile2 = self.__fileparser.beforeComparison(
                parsedFiles[i], parsedFiles[j]
            )
            distance = self.calculateFileSimilarity(measuringFile1, measuringFile2)
            self.__iterator.handleDistanceSetting(fileDistances, distance, i, j)

        return {
            "graphDistances": graphDistances,
            "fileDistances": fileDistances
        }
