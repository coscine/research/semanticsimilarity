from rdflib import ConjunctiveGraph, URIRef
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity

class FilterAdaptiveSimilarity(ISemanticSimilarity):
    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        
        datasets1 = list(graph1.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        datasets1.extend(list(graph1.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog"))))
        datasets2 = list(graph2.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        datasets2.extend(list(graph2.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog"))))

        both1 = 0
        for quad1 in graph1.quads():
            (s1, p1, o1, ctx1) = quad1
            for quad2 in graph2.quads((s1 if s1 not in datasets1 else None, p1, o1)):
                both1 += 1
                break
  
        both2 = 0
        for quad1 in graph2.quads():
            (s1, p1, o1, ctx1) = quad1
            for quad2 in graph1.quads((s1 if s1 not in datasets2 else None, p1, o1)):
                both2 += 1
                break

        bothCombined = both1 + both2

        ing1 = len(list(graph1.quads())) - both1
        ing2 = len(list(graph2.quads())) - both2

        return bothCombined / (bothCombined + ing1 + ing2)

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
