from rdflib import ConjunctiveGraph, URIRef
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity

class FilterStructureMemorySimilarity(ISemanticSimilarity):

    describedBy = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#describedby")
    hasPart = URIRef("http://purl.org/dc/terms/hasPart")
    dcatcatalog = URIRef("http://www.w3.org/ns/dcat#catalog")
    dcatdataset = URIRef("http://www.w3.org/ns/dcat#dataset")

    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        
        datasets1 = list(graph1.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        catalogs1 = list(graph1.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog")))
        datasets2 = list(graph2.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        catalogs2 = list(graph2.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog")))

        datasets1 = self.getQuadsOfSubs(datasets1, graph1)
        catalogs1 = self.getQuadsOfSubs(catalogs1, graph1)
        datasets2 = self.getQuadsOfSubs(datasets2, graph2)
        catalogs2 = self.getQuadsOfSubs(catalogs2, graph2)

        currentPercentage = 0
        currentMeasured = 0

        currentPercentage, currentMeasured = self.calculatePercentageOfSimilarDatasets(graph1, graph2, catalogs1, catalogs2, datasets1, datasets2, currentPercentage, currentMeasured)
        currentPercentage, currentMeasured = self.calculatePercentageOfSimilarDatasets(graph2, graph1, catalogs2, catalogs1, datasets2, datasets1, currentPercentage, currentMeasured)

        if currentMeasured != 0:
            currentPercentage = currentPercentage / currentMeasured

        return currentPercentage

    def getQuadsOfSubs(self, sets: 'list', graph: "ConjunctiveGraph"):
        dictionary = {}
        for setEntry in sets:
            dictionary[setEntry] = list(graph.quads((setEntry, None, None)))
        return dictionary

    def calculatePercentageOfSimilarDatasets(self, graph1, graph2, catalogs1, catalogs2, datasets1, datasets2, currentPercentage = 0, currentMeasured = 0):
        for key, value in catalogs1.items():
            
            currentPercentage, currentMeasured = self.getBestMatch(graph2, catalogs2, value, currentPercentage, currentMeasured)

            #for quad1 in [a for a in value if a[1] == self.dcatcatalog]:
                #(_, _, o1, _) = quad1
                #dataset1 = datasets1[o1]
                #currentPercentage, currentMeasured = self.getBestMatch(graph2, datasets2, dataset1, currentPercentage, currentMeasured)

            for quad1 in [a for a in value if a[1] == self.dcatdataset]:
                (_, _, o1, _) = quad1
                dataset1 = datasets1[o1]
                currentPercentage, currentMeasured = self.getBestMatch(graph2, datasets2, dataset1, currentPercentage, currentMeasured)
        return currentPercentage, currentMeasured

    def getBestMatch(self, graph2, catalogs2, catalog: 'list', currentPercentage = 0, currentMeasured = 0):
        bestMatch = 0
        for key, value in catalogs2.items():
            currentMatchPercentage = 0
            currentMatchMeasured = 0
            for quad1 in catalog:
                (_, p1, o1, _) = quad1
                if p1 == self.describedBy:
                    continue
                if p1 == self.hasPart:
                    continue
                if p1 == self.dcatdataset:
                    continue
                if p1 == self.dcatcatalog:
                    continue
                for _ in [a for a in value if a[1] == p1 and a[2] == o1]:
                    currentMatchPercentage += 1
                    break
                currentMatchMeasured += 1
            if currentMatchMeasured != 0:
                currentMatchPercentage = currentMatchPercentage / currentMatchMeasured
            else:
                bestMatch = 1
            if currentMatchPercentage > bestMatch:
                bestMatch = currentMatchPercentage
            if bestMatch == 1:
                break
        currentPercentage += bestMatch
        currentMeasured += 1
        return currentPercentage, currentMeasured

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
