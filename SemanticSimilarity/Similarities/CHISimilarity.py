from rdflib import Graph
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
import os
import hashlib

# The changed indicators (hash, modified, version) are implemented here

# BUF_SIZE is totally arbitrary, change for your app!
BUF_SIZE = 65536  # lets read stuff in 64kb chunks!

class CHIHashSimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__()

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        return 1

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1 if self.__getFileHash(file1) == self.__getFileHash(file2) else 0

    def __getFileHash(self, file):
        sha1 = hashlib.sha1()
        with open(file, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                sha1.update(data)
        return sha1.hexdigest()

class CHIModifiedSimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__()

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        return 1

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1 if os.path.getmtime(file1) == os.path.getmtime(file2) else 0

class CHISizeSimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__()

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        return 1

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1 if os.path.getsize(file1) == os.path.getsize(file2) else 0
