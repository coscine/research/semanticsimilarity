from rdflib import ConjunctiveGraph
from rdflib.term import URIRef
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
import logging

log = logging.getLogger(__name__)

baseIri = "https://purl.org/combination/"

class SPARQLSimplifiedSimilarity(ISemanticSimilarity):
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        combinedGraph = ConjunctiveGraph()

        count = [0]
        log.debug("Get Contexts 1")
        contextList1 = self.__set_contexts(graph1, combinedGraph, "?g1", count)
        log.debug("Get Contexts 2")
        contextList2 = self.__set_contexts(graph2, combinedGraph, "?g2", count)

        log.debug("Get Combined")
        both = self.__get_combined(combinedGraph, contextList1, contextList2)
        
        log.debug("Get in One")
        ing1 = self.__get_unique_combinations_1(combinedGraph, contextList1) - both
        log.debug("Get in Two")
        ing2 = self.__get_unique_combinations_2(combinedGraph, contextList2) - both

        return both / (both + ing1 + ing2)

    def __get_combined(self, combinedGraph: "ConjunctiveGraph", contextList1: "str", contextList2: "str"):
        query = """SELECT DISTINCT ?p ?o WHERE { 
    GRAPH ?g1 { ?s1 ?p ?o } .
    GRAPH ?g2 { ?s2 ?p ?o } .
    FILTER (""" + contextList1 + """) .
    FILTER (""" + contextList2 + """) .
  }"""
        return len(combinedGraph.query(query))

    def __get_unique_combinations_1(self, combinedGraph: "ConjunctiveGraph", contextList1: "str"):
        query = """SELECT DISTINCT ?p ?o WHERE { 
    GRAPH ?g1 { ?s1 ?p ?o } .
    FILTER (""" + contextList1 + """) .
  }"""
        return len(combinedGraph.query(query))

    def __get_unique_combinations_2(self, combinedGraph: "ConjunctiveGraph", contextList2: "str"):
        query = """SELECT DISTINCT ?p ?o WHERE { 
    GRAPH ?g2 { ?s2 ?p ?o } .
    FILTER (""" + contextList2 + """) .
  }"""
        return len(combinedGraph.query(query))

    def __set_contexts(self, graph: "ConjunctiveGraph", combinedGraph: "ConjunctiveGraph", graphIdentifier: "str", count: "list[int]"):
        graphName = baseIri + str(count[0])
        graphURIRef = URIRef(graphName)
        additionalGraph = combinedGraph.get_context(graphURIRef)
        for context in graph.contexts():
            for triple in context:
                additionalGraph.add(triple)
        count[0] += 1
        return graphIdentifier + " = <" + graphName + ">"

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
        