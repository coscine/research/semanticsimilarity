from rdflib import ConjunctiveGraph, URIRef
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity

class FilterStructureSimilarity(ISemanticSimilarity):

    describedBy = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#describedby")
    hasPart = URIRef("http://purl.org/dc/terms/hasPart")
    dcatcatalog = URIRef("http://www.w3.org/ns/dcat#catalog")
    dcatdataset = URIRef("http://www.w3.org/ns/dcat#dataset")

    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        
        datasets1 = list(graph1.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        catalogs1 = list(graph1.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog")))
        datasets2 = list(graph2.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        catalogs2 = list(graph2.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog")))

        currentPercentage = 0
        currentMeasured = 0

        currentPercentage, currentMeasured = self.calculatePercentageOfSimilarDatasets(graph1, graph2, catalogs1, catalogs2, datasets2, currentPercentage, currentMeasured)
        currentPercentage, currentMeasured = self.calculatePercentageOfSimilarDatasets(graph2, graph1, catalogs2, catalogs1, datasets1, currentPercentage, currentMeasured)

        if currentMeasured != 0:
            currentPercentage = currentPercentage / currentMeasured

        return currentPercentage

    def calculatePercentageOfSimilarDatasets(self, graph1, graph2, catalogs1, catalogs2, datasets2, currentPercentage = 0, currentMeasured = 0):
        for catalog in catalogs1:
            
            givenCatalog = graph1.quads((catalog, None, None))
            currentPercentage, currentMeasured = self.getBestMatch(graph2, catalogs2, list(givenCatalog), currentPercentage, currentMeasured)

            for quad1 in graph1.quads((catalog, self.dcatcatalog, None)):
                (_, _, o1, _) = quad1
                dataset1 = graph1.quads((o1, None, None))
                currentPercentage, currentMeasured = self.getBestMatch(graph2, datasets2, list(dataset1), currentPercentage, currentMeasured)

            for quad1 in graph1.quads((catalog, self.dcatdataset, None)):
                (_, _, o1, _) = quad1
                dataset1 = graph1.quads((o1, None, None))
                currentPercentage, currentMeasured = self.getBestMatch(graph2, datasets2, list(dataset1), currentPercentage, currentMeasured)
        return currentPercentage, currentMeasured

    def getBestMatch(self, graph2, catalogs2, catalog: 'list', currentPercentage = 0, currentMeasured = 0):
        bestMatch = 0
        for catalog2 in catalogs2:
            currentMatchPercentage = 0
            currentMatchMeasured = 0
            for quad1 in catalog:
                (_, p1, o1, _) = quad1
                if p1 == self.describedBy:
                    continue
                if p1 == self.hasPart:
                    continue
                if p1 == self.dcatdataset:
                    continue
                if p1 == self.dcatcatalog:
                    continue
                for _ in graph2.quads((catalog2, p1, o1)):
                    currentMatchPercentage += 1
                    break
                currentMatchMeasured += 1
            if currentMatchMeasured != 0:
                currentMatchPercentage = currentMatchPercentage / currentMatchMeasured
            else:
                bestMatch = 1
            if currentMatchPercentage > bestMatch:
                bestMatch = currentMatchPercentage
            if bestMatch == 1:
                break
        currentPercentage += bestMatch
        currentMeasured += 1
        return currentPercentage, currentMeasured

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
