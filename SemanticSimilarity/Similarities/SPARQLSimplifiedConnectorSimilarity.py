from rdflib import ConjunctiveGraph
from rdflib.graph import Graph
from rdflib.term import URIRef
from rdflib.plugins.stores.sparqlstore import SPARQLUpdateStore
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
import logging

log = logging.getLogger(__name__)

baseIri = "https://purl.org/combination/"
endpoint = "http://localhost:8890/sparql"
loadFile = "load.trig"
loadPath = "C:/voc/"
loadPathSyntax = "file:///voc/"

# Requires a Virtuoso instance to run
class SPARQLSimplifiedConnectorSimilarity(ISemanticSimilarity):
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):

        updateStore = SPARQLUpdateStore(endpoint)
        gs = ConjunctiveGraph(updateStore)

        log.debug("Open Connection")
        gs.open((endpoint, endpoint))

        log.debug("Cleanup")
        # Do initial cleanup so that no rest data is being used
        self.__cleanup(gs, [len(list(graph1.contexts())) + len(list(graph2.contexts()))])

        count = [0]
        log.debug("Get Contexts 1")
        contextList1 = self.__set_contexts(graph1, gs, "?g1", count)
        log.debug("Get Contexts 2")
        contextList2 = self.__set_contexts(graph2, gs, "?g2", count)

        log.debug("Get Combined")
        both = self.__get_combined(gs, contextList1, contextList2)
        
        log.debug("Get in One")
        ing1 = self.__get_unique_combinations_1(gs, contextList1) - both
        log.debug("Get in Two")
        ing2 = self.__get_unique_combinations_2(gs, contextList2) - both

        log.debug("Cleanup")
        self.__cleanup(gs, count)

        log.debug("Close Connection")
        gs.close()

        return both / (both + ing1 + ing2)

    def __get_combined(self, combinedGraph: "ConjunctiveGraph", contextList1: "str", contextList2: "str"):
        query = """SELECT DISTINCT ?p ?o WHERE { 
    GRAPH ?g1 { ?s1 ?p ?o } .
    GRAPH ?g2 { ?s2 ?p ?o } .
    FILTER (""" + contextList1 + """) .
    FILTER (""" + contextList2 + """) .
  }"""
        return len(combinedGraph.query(query))

    def __get_unique_combinations_1(self, combinedGraph: "ConjunctiveGraph", contextList1: "str"):
        query = """SELECT DISTINCT ?p ?o WHERE { 
    GRAPH ?g1 { ?s1 ?p ?o } .
    FILTER (""" + contextList1 + """) .
  }"""
        return len(combinedGraph.query(query))

    def __get_unique_combinations_2(self, combinedGraph: "ConjunctiveGraph", contextList2: "str"):
        query = """SELECT DISTINCT ?p ?o WHERE { 
    GRAPH ?g2 { ?s2 ?p ?o } .
    FILTER (""" + contextList2 + """) .
  }"""
        return len(combinedGraph.query(query))

    def __set_contexts(self, graph: "ConjunctiveGraph", combinedGraph: "ConjunctiveGraph", graphIdentifier: "str", count: "list[int]"):
        graphName = baseIri + str(count[0])
        graphURIRef = URIRef(graphName)
        tempGraph = ConjunctiveGraph()
        addGraph = tempGraph.get_context(graphURIRef)
        #combinedGraph.query("CREATE GRAPH <" + graphName + ">")
        for context in graph.contexts():
            for triple in context:
                #(subject, predicate, obj) = triple
                #triple = "%s %s %s ." % (subject.n3(), predicate.n3(), obj.n3())
                #q = "INSERT DATA { GRAPH %s { %s } }" % (
                 #   graphURIRef.n3(), triple)
                #combinedGraph.query(q)
                addGraph.add(triple)
        loadFilePath = loadPath + loadFile
        loadFileSyntax = loadPathSyntax + loadFile
        tempGraph.serialize(loadFilePath, format="trig", encoding="utf-8")
        combinedGraph.query("LOAD <" + loadFileSyntax + ">")
        count[0] += 1
        return graphIdentifier + " = <" + graphName + ">"

    def __cleanup(self, combinedGraph: "ConjunctiveGraph", count: "list[int]"):
        for i in range(count[0]):
            graphName = baseIri + str(i)
            combinedGraph.query("DROP SILENT GRAPH <" + graphName + ">")

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
        