from SemanticSimilarity.GraphParser.NetworkxGraphParser import NetworkxGraphParser
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
from networkx.algorithms.similarity import simrank_similarity, optimize_graph_edit_distance, graph_edit_distance
from networkx import Graph
import logging

log = logging.getLogger(__name__)

# NP Hard problem
# Very slow
class GraphEditDistanceSimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__(graphparser=NetworkxGraphParser())

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        distance = 0
        for v in graph_edit_distance(graph1, graph2):
            distance = v
        log.debug('Distance: ' + str(distance))
        return 1 - distance

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1

# Very slow
class OptimizeGraphEditDistanceSimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__(graphparser=NetworkxGraphParser())

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        distance = 0
        for v in optimize_graph_edit_distance(graph1, graph2):
            distance = v
        log.debug('Distance: ' + str(distance))
        return 1 - distance

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1

# Not working
class SimRankSimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__(graphparser=NetworkxGraphParser())

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        distance = simrank_similarity(graph1, graph2)
        return distance

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
