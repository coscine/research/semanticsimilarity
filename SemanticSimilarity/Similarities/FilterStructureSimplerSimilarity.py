from SemanticSimilarity.GraphParser.NoiseGraphTransformParser import NoiseGraphTransformParser
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
import multiprocessing as mp

class FilterStructureSimplerSimilarity(ISemanticSimilarity):

    currentPercentage = 0
    currentMeasured = 0

    def __init__(self, graphparser=NoiseGraphTransformParser()):
        super().__init__(graphparser=graphparser)

    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """
    def calculateGraphSimilarity(self, 
        graph1: "tuple(list[set[tuple]], list[set[tuple]], list[set[tuple]], list[set[tuple]])", 
        graph2: "tuple(list[set[tuple]], list[set[tuple]], list[set[tuple]], list[set[tuple]])"):
        
        subjects1, datasets1, catalogs1, facts1 = graph1
        subjects2, datasets2, catalogs2, facts2 = graph2

        self.currentPercentage = 0
        self.currentMeasured = 0

        self.calculatePercentageOfSimilarDatasets(subjects1, subjects2)
        self.calculatePercentageOfSimilarDatasets(subjects2, subjects1)
        self.calculatePercentageOfSimilarDatasets(catalogs1, catalogs2)
        self.calculatePercentageOfSimilarDatasets(catalogs2, catalogs1)
        self.calculatePercentageOfSimilarDatasets(datasets1, datasets2)
        self.calculatePercentageOfSimilarDatasets(datasets2, datasets1)
        self.calculatePercentageOfSimilarDatasets(facts1, facts2)
        self.calculatePercentageOfSimilarDatasets(facts2, facts1)

        if self.currentMeasured != 0:
           self.currentPercentage = self.currentPercentage / self.currentMeasured

        return self.currentPercentage

    def calculatePercentageOfSimilarDatasets(self, catalogs1: "list[set[tuple]]", catalogs2: "list[set[tuple]]"):
        print("Calculate percentages of " + str(len(catalogs1)) + " entries")
        if len(catalogs1) > mp.cpu_count() and len(catalogs2) > mp.cpu_count():
            pool = mp.Pool(mp.cpu_count())
            results = pool.starmap(self.getBestMatch, [(catalogs2, value) for value in catalogs1])
            pool.close()
        else:
            results = [self.getBestMatch(catalogs2, value) for value in catalogs1]
        self.currentMeasured += sum([result[1] for result in results])
        self.currentPercentage += sum([result[0] * result[1] for result in results])

    def getBestMatch(self, catalogs2: "list[set[tuple]]", catalog: 'set[tuple]'):
        bestMatch = 0
        currentMatchMeasured = len(catalog)
        if currentMatchMeasured != 0:
            for value in catalogs2:
                currentMatchPercentage = len(catalog.intersection(value))
                currentMatchPercentage = currentMatchPercentage / currentMatchMeasured
                if currentMatchPercentage > bestMatch:
                    bestMatch = currentMatchPercentage
                if bestMatch == 1:
                    break
        return bestMatch, currentMatchMeasured

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
