import numpy as np
from rdflib import ConjunctiveGraph
from rdflib.compare import graph_diff
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity


class CustomSemanticSimilarity(ISemanticSimilarity):
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        both, ing1, ing2 = graph_diff(graph1, graph2)
        distance = len(both) / (len(both) + len(ing1) + len(ing2))
        # distance = self.__logisticResult(distance)
        return distance

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1

    def __logisticResult(self, t):
        A = 0
        K = 1
        C = 1
        Q = 1
        B = 5
        v = 1

        logResult = A + ((K - A) / ((C + Q * np.exp(-B * t)) ** (1 / v)))
        return (logResult - 0.5) * 2
