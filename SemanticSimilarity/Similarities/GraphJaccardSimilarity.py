from SemanticSimilarity.GraphParser.NoiseGraphTransformParser import NoiseGraphTransformParser
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
import multiprocessing as mp
import itertools

class GraphJaccardSimilarity(ISemanticSimilarity):

    currentPercentage = 0
    currentMeasured = 0

    def __init__(self, graphparser=NoiseGraphTransformParser()):
        super().__init__(graphparser=graphparser)

    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """
    def calculateGraphSimilarity(self, 
        graph1: "tuple(list[set[tuple]], list[set[tuple]], list[set[tuple]], list[set[tuple]])", 
        graph2: "tuple(list[set[tuple]], list[set[tuple]], list[set[tuple]], list[set[tuple]])"):
        
        subjects1, datasets1, catalogs1, facts1 = graph1
        subjects2, datasets2, catalogs2, facts2 = graph2

        self.currentPercentage = 0

        graph1Union = set(itertools.chain.from_iterable(subjects1)).union(set(itertools.chain.from_iterable(datasets1))).union(set(itertools.chain.from_iterable(catalogs1))).union(set(itertools.chain.from_iterable(facts1)))
        graph2Union = set(itertools.chain.from_iterable(subjects2)).union(set(itertools.chain.from_iterable(datasets2))).union(set(itertools.chain.from_iterable(catalogs2))).union(set(itertools.chain.from_iterable(facts2)))

        self.currentPercentage = len(graph1Union.intersection(graph2Union)) / len(graph1Union.union(graph2Union))

        return self.currentPercentage

    def calculatePercentageOfSimilarDatasets(self, catalogs1: "list[set[tuple]]", catalogs2: "list[set[tuple]]"):
        print("Calculate percentages of " + str(len(catalogs1)) + " entries")
        if len(catalogs1) > mp.cpu_count() and len(catalogs2) > mp.cpu_count():
            pool = mp.Pool(mp.cpu_count())
            results = pool.starmap(self.getBestMatch, [(catalogs2, value) for value in catalogs1])
            pool.close()
        else:
            results = [self.getBestMatch(catalogs2, value) for value in catalogs1]
        self.currentMeasured += len(results)
        self.currentPercentage += sum(results)

    # Determines the best match based on the Jaccard distance between multiple sets
    def getBestMatch(self, catalogs2: "list[set[tuple]]", catalog: 'set[tuple]'):
        bestMatch = 0
        currentMatchMeasured = len(catalog)
        if currentMatchMeasured == 0:
            return 1
        for value in catalogs2:
            currentMatchPercentage = len(catalog.intersection(value)) / len(catalog.union(value))
            if currentMatchPercentage > bestMatch:
                bestMatch = currentMatchPercentage
            if bestMatch == 1:
                break
        return bestMatch

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
