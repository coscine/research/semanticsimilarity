from rdflib import ConjunctiveGraph
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity

class FilterSimilarity(ISemanticSimilarity):
    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """
    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        
        both1 = 0
        for quad1 in graph1.quads():
            (s1, p1, o1, ctx1) = quad1
            wentIn = False
            for quad2 in graph2.quads((None, p1, o1)):
                both1 += 1
                wentIn = True
                break
            #if not wentIn:
                #print((str(s1), str(p1), str(o1), str(ctx1)))
  
        both2 = 0
        for quad1 in graph2.quads():
            (s1, p1, o1, ctx1) = quad1
            wentIn = False
            for quad2 in graph1.quads((None, p1, o1)):
                both2 += 1
                wentIn = True
                break
            #if not wentIn:
                #print((str(s1), str(p1), str(o1), str(ctx1)))

        bothCombined = both1 + both2

        ing1 = len(list(graph1.quads())) - both1
        ing2 = len(list(graph2.quads())) - both2

        return bothCombined / (bothCombined + ing1 + ing2)

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
