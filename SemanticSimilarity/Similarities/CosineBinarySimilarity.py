from rdflib import Graph
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
from SemanticSimilarity.FileParser.BinaryFileParser import BinaryFileParser
from scipy import spatial

class CosineBinarySimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__(fileparser=BinaryFileParser())

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        return 1

    def calculateFileSimilarity(self, byteList1, byteList2):
        return self.__cosine_binary(byteList1, byteList2)

    def __cosine_binary(self, x, y):
        """A function for finding the similarity between two binary vectors"""
        return 1 - spatial.distance.cosine(x, y)
