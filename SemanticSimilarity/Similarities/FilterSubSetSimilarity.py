from rdflib import ConjunctiveGraph
from SemanticSimilarity.Iterators.FullIterator import FullIterator
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity

class FilterSubSetSimilarity(ISemanticSimilarity):
    """
    This method calculates the predicate and object pairs for both graphs by iterating over them
    It has to calculate from both side, in case there is something like ?g1 = (?a ?p ?o, ?b ?p ?o) and ?g2 = (?c ?p ?o)
    where it would get a match length of two which doesn't make sense for ?g2.

    Therefore, both match lengths are calculated and combined together and calculated as a percentage by the formula:
    percentage = (bothBy1 + bothBy2) / (bothBy1 + bothBy2 + onlyIn1 + onlyIn2)
    """

    def __init__(self):
        super().__init__(iterator=FullIterator())

    def calculateGraphSimilarity(self, graph1: "ConjunctiveGraph", graph2: "ConjunctiveGraph"):
        
        both1 = 0
        for quad1 in graph1.quads():
            (s1, p1, o1, ctx1) = quad1
            for quad2 in graph2.quads((None, p1, o1)):
                both1 += 1
                break

        return both1 / len(list(graph1.quads()))

    def calculateFileSimilarity(self, file1: "str", file2: "str"):
        return 1
