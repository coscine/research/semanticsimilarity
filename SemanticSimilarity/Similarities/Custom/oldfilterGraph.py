from rdflib import URIRef

describedby = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#describedby")
value = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#value")
member = URIRef("http://www.w3.org/2000/01/rdf-schema#member")

fileRef = URIRef("https://purl.org/coscine/ontologies/file/identifier#compare")


def createSimilarityTriples(graph):
    graph.remove((None, describedby, None))
    graph.remove((None, value, None))
    graph.remove((None, member, None))
    # TODO: Rethink this
    for (subject, predicate, obj) in list(graph):
        subStr = str(subject)
        if "https://purl.org/coscine/ontologies/file/identifier#" in subStr:
            graph.remove((subject, predicate, obj))
            graph.add((fileRef, predicate, obj))
        elif "https://hdl.handle.net/21.11102/" in subStr:
            graph.remove((subject, predicate, obj))
            graph.add((fileRef, predicate, obj))
    for (subject, predicate, obj) in list(graph):
        objStr = str(obj)
        if "https://purl.org/coscine/ontologies/file/identifier#" in objStr:
            graph.remove((subject, predicate, obj))
            graph.add((subject, predicate, fileRef))
        elif "https://hdl.handle.net/21.11102/" in objStr:
            graph.remove((subject, predicate, obj))
            graph.add((subject, predicate, fileRef))
    for (subject, predicate, obj) in list(graph):
        subStr = str(subject)
        if "#" in subStr:
            subIdent = subStr[subStr.index("#") + 1 :]
            objStr = str(obj)
            if (
                "http://pikes.fbk.eu/#" in subStr
                or "http://staedtler_onto.org/youtube_description/_GSC1UMGO-o#"
                in subStr
            ) and "_" in subIdent:
                subStr = subStr[: subStr.rindex("_")]
                parsedSubj = URIRef(subStr)
                graph.remove((subject, predicate, obj))
                graph.add((parsedSubj, predicate, obj))
    for (subject, predicate, obj) in list(graph):
        subStr = str(subject)
        objStr = str(obj)
        if "#" in objStr:
            objIdent = objStr[objStr.index("#") + 1 :]
            if (
                "http://pikes.fbk.eu/#" in objStr
                or "http://staedtler_onto.org/youtube_description/_GSC1UMGO-o#"
                in objStr
            ) and "_" in objIdent:
                objStr = objStr[: objStr.rindex("_")]
                parsedObj = URIRef(objStr)
                graph.remove((subject, predicate, obj))
                graph.add((subject, predicate, parsedObj))
