from rdflib.compare import graph_diff
from rdflib import Graph, ConjunctiveGraph, URIRef
import numpy as np
import networkx as nx

# pip install python-louvain
import community

from datetime import datetime

import json
import pandas
import seaborn as sns
import matplotlib.pyplot as plt
import glob

import os, os.path

from oldfilterGraph import createSimilarityTriples

out_dir = "./distance_out"

if not os.path.exists(out_dir):
    os.makedirs(out_dir)

trigfiles = []
for trigfile in glob.glob("./output/*.trig"):
    trigfiles.append(trigfile)

# trigfiles = trigfiles[9:]

distances = np.identity(len(trigfiles), dtype=float)

graphs = []
for trigfile in trigfiles:
    graph = ConjunctiveGraph()
    graph.parse(trigfile, format="trig")
    createSimilarityTriples(graph)
    graphs.append(graph)


def logisticResult(t):
    A = 0
    K = 1
    C = 1
    Q = 1
    B = 5
    v = 1

    logResult = A + ((K - A) / ((C + Q * np.exp(-B * t)) ** (1 / v)))
    return (logResult - 0.5) * 2


for i in range(0, len(graphs)):
    for j in range(i + 1, len(graphs)):
        print(
            "Working on "
            + trigfiles[i]
            + " ("
            + str(i)
            + ") and "
            + trigfiles[j]
            + " ("
            + str(j)
            + ")!"
        )
        both, ing1, ing2 = graph_diff(graphs[i], graphs[j])
        distance = len(both) / (len(both) + len(ing1) + len(ing2))
        distance = logisticResult(distance)
        distances[i][j] = distance
        distances[j][i] = distance

print("")
print(trigfiles)
print("")

labels = [entry.replace("./output\\", "") for entry in trigfiles]

df = pandas.DataFrame(distances, columns=labels, index=labels)

df.to_csv(out_dir + "/diff.csv")
df
plt.autoscale()
sns.heatmap(df, annot=True)
plt.tight_layout()
plt.savefig(out_dir + "/heatmap.pdf")
plt.show()
plt.clf()

# TODO: Look into https://github.com/benedekrozemberczki/awesome-community-detection
# TODO: Use: https://cdlib.readthedocs.io/en/latest/index.html
# closeness = distances
# closeness[:][:] = 1 - closeness[:][:]

df = pandas.DataFrame(distances, columns=labels, index=labels)
G = nx.from_pandas_adjacency(df)
partition = community.best_partition(G)

# drawing
size = float(len(set(partition.values())))
pos = nx.spring_layout(G)
count = 0.0
for com in set(partition.values()):
    count = count + 1.0
    list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == com]
    nx.draw_networkx_nodes(
        G, pos, list_nodes, node_size=20, node_color=str(count / size)
    )


nx.draw_networkx_edges(G, pos, alpha=0.5)
with open(out_dir + "/partition.json", "w") as file:
    file.write(json.dumps(partition, indent=4))
plt.savefig(out_dir + "/communities.pdf")
# plt.show()

# Create relation graph

graphs = []
for trigfile in trigfiles:
    graph = ConjunctiveGraph()
    graph.parse(trigfile, format="trig")
    graphs.append(graph)

relationGraph = Graph()

resourceNamePredicate = URIRef("https://purl.org/coscine/ontologies/tika/resourceName")
# a = rdf:type
predicate = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
obj = URIRef("http://www.w3.org/ns/prov#Entity")

modified = URIRef("http://purl.org/dc/terms/modified")
last_modified = URIRef("https://purl.org/coscine/ontologies/tika/Last_Modified")
pdf_docinfo_modified = URIRef(
    "https://purl.org/coscine/ontologies/tika/pdf:docinfo:modified"
)
creation_date = URIRef("https://purl.org/coscine/ontologies/tika/Creation_Date")
meta_creation_date = URIRef(
    "https://purl.org/coscine/ontologies/tika/meta:creation_date"
)
pdf_docinfo_created = URIRef(
    "https://purl.org/coscine/ontologies/tika/pdf:docinfo:created"
)
created = URIRef("http://purl.org/dc/terms/created")

# same => URIRef("https://purl.org/coscine/ontologies/prov/extension#derivesFrom")
wasDerivedFrom = URIRef("http://www.w3.org/ns/prov#wasDerivedFrom")
relatesTo = URIRef("https://purl.org/coscine/ontologies/prov/extension#relatesTo")
similarTo = URIRef("https://purl.org/coscine/ontologies/prov/extension#similarTo")
topicRelated = URIRef("https://purl.org/coscine/ontologies/prov/extension#topicRelated")
vaguelyRelated = URIRef(
    "https://purl.org/coscine/ontologies/prov/extension#vaguelyRelated"
)
notRelated = URIRef("https://purl.org/coscine/ontologies/prov/extension#notRelated")


def getSubject(graph: ConjunctiveGraph):
    return graph.default_context.identifier
    for tmpSubject, tmpPredicate, tmpObject in graph.triples(
        (None, resourceNamePredicate, None)
    ):
        return tmpSubject
    return None


def getResourceName(graph):
    return graph.default_context.identifier
    for tmpSubject, tmpPredicate, tmpObject in graph.triples(
        (None, resourceNamePredicate, None)
    ):
        return tmpObject
    return None


def parseDateByPredicate(graph, subject, predicate):
    for tmpSubject, tmpPredicate, tmpObject in graph.triples(
        (subject, predicate, None)
    ):
        strObject = str(tmpObject)
        if "Z" in strObject:
            return datetime.strptime(strObject, "%Y-%m-%dT%H:%M:%SZ")
        else:
            return datetime.strptime(strObject, "%Y-%m-%dT%H:%M:%S")
    return None


def getModifiedDate(graph, subject):
    modifiedDate = parseDateByPredicate(graph, subject, modified)
    if modifiedDate == None:
        modifiedDate = parseDateByPredicate(graph, subject, last_modified)
    if modifiedDate == None:
        modifiedDate = parseDateByPredicate(graph, subject, pdf_docinfo_modified)
    if modifiedDate == None:
        modifiedDate = parseDateByPredicate(graph, subject, creation_date)
    if modifiedDate == None:
        modifiedDate = parseDateByPredicate(graph, subject, meta_creation_date)
    if modifiedDate == None:
        modifiedDate = parseDateByPredicate(graph, subject, pdf_docinfo_created)
    if modifiedDate == None:
        modifiedDate = parseDateByPredicate(graph, subject, created)
    return modifiedDate


for i in range(0, len(graphs)):
    graph = graphs[i]

    subject = getSubject(graph)

    modifiedDate = getModifiedDate(graph, subject)

    resourceName = getResourceName(graph)

    relationGraph.add((subject, predicate, obj))
    relationGraph.add((subject, resourceNamePredicate, resourceName))
    for j in range(i + 1, len(graphs)):
        otherGraph = graphs[j]

        otherSubject = getSubject(otherGraph)

        otherModifiedDate = getModifiedDate(otherGraph, otherSubject)

        otherResourceName = getResourceName(graph)

        firstSubject = subject
        nextSubject = otherSubject
        if otherModifiedDate != None and modifiedDate == None:
            nextSubject = subject
            firstSubject = otherSubject
        elif otherModifiedDate != None and modifiedDate != None:
            if otherModifiedDate > modifiedDate:
                nextSubject = subject
                firstSubject = otherSubject

        distance = distances[i][j]
        if distance >= 0.95:
            relationPredicate = wasDerivedFrom
        elif distance >= 0.8 and distance < 0.95:
            relationPredicate = relatesTo
        elif distance >= 0.6 and distance < 0.8:
            relationPredicate = similarTo
        elif distance >= 0.4 and distance < 0.96:
            relationPredicate = topicRelated
        elif distance >= 0.2 and distance < 0.4:
            relationPredicate = vaguelyRelated
        else:
            relationPredicate = notRelated

        relationGraph.add((firstSubject, relationPredicate, nextSubject))

relationTrig = relationGraph.serialize(format="trig", encoding="utf-8").decode("utf-8")

with open("relation.trig", "w", encoding="utf-8") as relation_file:
    relation_file.write(relationTrig)
