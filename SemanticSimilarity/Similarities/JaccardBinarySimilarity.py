import numpy as np
from rdflib import Graph
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
from SemanticSimilarity.FileParser.BinaryFileParser import BinaryFileParser

class JaccardBinarySimilarity(ISemanticSimilarity):
    def __init__(self):
        super().__init__(fileparser=BinaryFileParser())

    def calculateGraphSimilarity(self, graph1: "Graph", graph2: "Graph"):
        return 1

    def calculateFileSimilarity(self, byteList1, byteList2):
        return self.__jaccard_binary(byteList1, byteList2)

    def __jaccard_binary(self, x, y):
        """A function for finding the similarity between two binary vectors"""
        intersection = np.logical_and(x, y)
        union = np.logical_or(x, y)
        similarity = intersection.sum() / float(union.sum())
        return similarity
