from abc import ABCMeta, abstractmethod

import logging
from typing import Generator

log = logging.getLogger(__name__)


class IIterator:
    __metaclass__ = ABCMeta

    @abstractmethod
    def generateIterator(self, list: "list") -> "Generator[tuple[int, int], None, None]":
        raise NotImplementedError

    @abstractmethod
    def handleDistanceSetting(self, distances, distance: float, i: int, j: int) -> None:
        raise NotImplementedError
