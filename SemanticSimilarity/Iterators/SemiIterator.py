from typing import Generator
from SemanticSimilarity.Iterators.IIterator import IIterator

import logging

log = logging.getLogger(__name__)


class SemiIterator(IIterator):

    def generateIterator(self, list: "list") -> "Generator[tuple[int, int], None, None]":
        for i in range(0, len(list)):
            for j in range(i, len(list)):
                yield (i, j)

    def handleDistanceSetting(self, distances, distance: float, i: int, j: int) -> None:
        distances[i][j] = distance
        distances[j][i] = distance