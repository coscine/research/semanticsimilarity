from SemanticSimilarity.GraphParser.IGraphParser import IGraphParser
from rdflib import ConjunctiveGraph, Graph, URIRef

class NoiseGraphParser(IGraphParser):

    describedBy = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#describedby")
    hasPart = URIRef("http://purl.org/dc/terms/hasPart")
    dcatcatalog = URIRef("http://www.w3.org/ns/dcat#catalog")
    dcatdataset = URIRef("http://www.w3.org/ns/dcat#dataset")
    foafdepiction = URIRef("http://xmlns.com/foaf/0.1/depiction")

    def beforeComparison(self, graph1, graph2):
        return graph1, graph2

    def parseGraph(self, originalgraph: "Graph"):
        graph = ConjunctiveGraph()
        graph.parse(data=originalgraph.serialize(format='trig', encoding="utf-8"), format='trig')
        self.cleanGraph(graph)
        return graph

    def cleanGraph(self, graph: "ConjunctiveGraph"):
        graph.remove((None, self.describedBy, None))
        graph.remove((None, self.hasPart, None))
        graph.remove((None, self.dcatdataset, None))
        graph.remove((None, self.dcatcatalog, None))
        graph.remove((None, self.foafdepiction, None))
