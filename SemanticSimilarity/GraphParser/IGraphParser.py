from rdflib import Graph
from abc import ABCMeta, abstractmethod

import logging

log = logging.getLogger(__name__)


class IGraphParser:
    __metaclass__ = ABCMeta

    @abstractmethod
    def beforeComparison(self, graph1, graph2):
        raise NotImplementedError

    @abstractmethod
    def parseGraph(self, graph: "Graph"):
        raise NotImplementedError
