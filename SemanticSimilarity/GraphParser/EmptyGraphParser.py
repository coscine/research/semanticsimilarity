from SemanticSimilarity.GraphParser.IGraphParser import IGraphParser
from rdflib import Graph

class EmptyGraphParser(IGraphParser):
    def beforeComparison(self, graph1, graph2):
        return graph1, graph2

    def parseGraph(self, graph: "Graph"):
        return graph
