from SemanticSimilarity.GraphParser.IGraphParser import IGraphParser
from rdflib import ConjunctiveGraph, Graph, URIRef

class NoiseGraphTransformParserOnlyDescriptive(IGraphParser):

    describedBy = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#describedby")
    hasPart = URIRef("http://purl.org/dc/terms/hasPart")
    dcatcatalog = URIRef("http://www.w3.org/ns/dcat#catalog")
    dcatdataset = URIRef("http://www.w3.org/ns/dcat#dataset")
    foafdepiction = URIRef("http://xmlns.com/foaf/0.1/depiction")

    def beforeComparison(self, graph1, graph2):
        return graph1, graph2

    def parseGraph(self, originalgraph: "Graph") -> "tuple(list[set[tuple]], list[set[tuple]], list[set[tuple]])":
        graph = ConjunctiveGraph()
        graph.parse(data=originalgraph.serialize(format='trig', encoding="utf-8"), format='trig')
        self.cleanGraph(graph)

        dataContext = None
        for context in graph.contexts():
            if "type=data" in str(context.identifier):
                dataContext = context
                break

        if dataContext != None:
            graph.remove_context(dataContext)

        subjects = set(graph.subjects())
        datasets = set(graph.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        catalogs = set(graph.subjects(predicate=URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), object=URIRef("http://www.w3.org/ns/dcat#Catalog")))
        
        subjects = subjects.difference(datasets).difference(catalogs)

        subjects = self.getQuadsOfSubs(subjects, graph)
        datasets = self.getQuadsOfSubs(datasets, graph)
        catalogs = self.getQuadsOfSubs(catalogs, graph)

        return (subjects, datasets, catalogs)

    def cleanGraph(self, graph: "ConjunctiveGraph"):
        graph.remove((None, self.describedBy, None))
        graph.remove((None, self.hasPart, None))
        graph.remove((None, self.dcatdataset, None))
        graph.remove((None, self.dcatcatalog, None))
        graph.remove((None, self.foafdepiction, None))

    def clearSubjects(self, subjects: 'list', datasets: 'list', catalogs: 'list'):
        toRemove = []
        for subject in subjects:
            if subject in datasets or subject in catalogs:
                toRemove.append(subject)
        for removable in toRemove:
            subjects.remove(removable)

    def getQuadsOfSubs(self, sets: 'set', graph: "ConjunctiveGraph"):
        return [set(graph.predicate_objects(setEntry)) for setEntry in sets]