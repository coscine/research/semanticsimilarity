from SemanticSimilarity.GraphParser.IGraphParser import IGraphParser
from rdflib import ConjunctiveGraph, Graph, URIRef
import itertools

class NoiseGraphTransformParser(IGraphParser):

    describedBy = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#describedby")
    hasPart = URIRef("http://purl.org/dc/terms/hasPart")
    dcatcatalog = URIRef("http://www.w3.org/ns/dcat#catalog")
    dcatdataset = URIRef("http://www.w3.org/ns/dcat#dataset")
    foafdepiction = URIRef("http://xmlns.com/foaf/0.1/depiction")
    rdfType = URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")

    def beforeComparison(self, graph1, graph2):
        return graph1, graph2

    def parseGraph(self, originalgraph: "Graph") -> "tuple(list[set[tuple]], list[set[tuple]], list[set[tuple]], list[set[tuple]])":
        graph = ConjunctiveGraph()
        parsedData = originalgraph.serialize(format='trig', encoding="utf-8")
        # Patch RDF in, otherwise it sometimes breaks
        parsedData = b'@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n' + parsedData
        graph.parse(data=parsedData, format='trig')
        self.cleanGraph(graph)

        factContexts = set(filter(lambda x: "https://purl.org/coscine/ontologies/factgraph/" in x.identifier, graph.contexts()))

        subjects = set(graph.subjects())
        datasets = set(graph.subjects(predicate=self.rdfType, object=URIRef("http://www.w3.org/ns/dcat#Dataset")))
        catalogs = set(graph.subjects(predicate=self.rdfType, object=URIRef("http://www.w3.org/ns/dcat#Catalog")))
        facts = set(itertools.chain.from_iterable([list(factContext.subjects()) for factContext in factContexts][:]))
        
        subjects = subjects.difference(datasets).difference(catalogs).difference(facts)

        subjects = self.getQuadsOfSubs(subjects, graph)
        datasets = self.getQuadsOfSubs(datasets, graph, True)
        catalogs = self.getQuadsOfSubs(catalogs, graph, True)
        facts = list([set(factContext) for factContext in factContexts])
        
        return (subjects, datasets, catalogs, facts)

    def cleanGraph(self, graph: "ConjunctiveGraph"):
        graph.remove((None, self.describedBy, None))
        graph.remove((None, self.hasPart, None))
        graph.remove((None, self.dcatdataset, None))
        graph.remove((None, self.dcatcatalog, None))
        graph.remove((None, self.foafdepiction, None))

    def clearSubjects(self, subjects: 'list', datasets: 'list', catalogs: 'list'):
        toRemove = []
        for subject in subjects:
            if subject in datasets or subject in catalogs:
                toRemove.append(subject)
        for removable in toRemove:
            subjects.remove(removable)

    def getQuadsOfSubs(self, sets: 'set', graph: "ConjunctiveGraph", removeInstanceDeclaration = False):
        return [set(filter(lambda x: not removeInstanceDeclaration or x[0] != self.rdfType, graph.predicate_objects(setEntry))) for setEntry in sets]
