from SemanticSimilarity.GraphParser.IGraphParser import IGraphParser
from rdflib import Graph
from rdflib.extras.external_graph_libs import rdflib_to_networkx_graph

class NetworkxGraphParser(IGraphParser):
    def beforeComparison(self, graph1, graph2):
        return graph1, graph2

    def parseGraph(self, graph: "Graph"):
        return rdflib_to_networkx_graph(graph)
