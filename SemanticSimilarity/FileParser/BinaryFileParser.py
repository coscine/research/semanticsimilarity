import numpy as np
from SemanticSimilarity.FileParser.IFileParser import IFileParser


class BinaryFileParser(IFileParser):
    def beforeComparison(self, byteList1, byteList2):
        size1 = byteList1.size
        size2 = byteList2.size
        maxSize = max(size1, size2)
        if size1 < maxSize:
            byteList1 = np.pad(
                byteList1, (0, maxSize - size1), "constant", constant_values=(0)
            )
        if size2 < maxSize:
            byteList2 = np.pad(
                byteList2, (0, maxSize - size2), "constant", constant_values=(0)
            )
        return byteList1, byteList2

    def parseFile(self, file: "str"):
        return np.fromfile(file, dtype='B')
