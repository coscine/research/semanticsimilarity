from abc import ABCMeta, abstractmethod

import logging

log = logging.getLogger(__name__)


class IFileParser:
    __metaclass__ = ABCMeta

    @abstractmethod
    def beforeComparison(self, file1, file2):
        raise NotImplementedError

    @abstractmethod
    def parseFile(self, file: "str"):
        raise NotImplementedError
