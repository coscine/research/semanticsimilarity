from SemanticSimilarity.FileParser.IFileParser import IFileParser


class EmptyFileParser(IFileParser):
    def beforeComparison(self, file1, file2):
        return file1, file2

    def parseFile(self, file: "str"):
        return file
