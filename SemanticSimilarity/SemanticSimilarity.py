from SemanticSimilarity.Similarities.FilterStructureSimplerSimilarity import (
    FilterStructureSimplerSimilarity,
)
from SemanticSimilarity.Similarities.ISemanticSimilarity import ISemanticSimilarity
from rdflib import ConjunctiveGraph
import time
import logging
from numpy import ndarray

log = logging.getLogger(__name__)

class SimilarityReturnObject:
    similarity_matrix: dict[str, ndarray]
    time_spent: float

class SimilarityObject:
    method: str
    returnObject: SimilarityReturnObject

class SemanticSimilarity:
    def __init__(
        self,
        methods: "list[ISemanticSimilarity]" = [
            FilterStructureSimplerSimilarity()
        ],
    ):
        self.__methods = methods

    def getSimilarities(self, graphs: "list[ConjunctiveGraph]" = [], files: "list[str]" = []) -> list[SimilarityObject]:
        similarities = []
        for method in self.__methods:
            start_time = time.time()
            filteredGraphs, filteredFiles = method.preprocess(graphs, files)
            similarity_matrix = method.calculateSimilarityMatrix(
                filteredGraphs, filteredFiles
            )
            end_time = time.time()
            time_spent = end_time - start_time
            similarities.append({
                "method": type(method).__name__,
                "returnObject": {
                  "similarity_matrix": similarity_matrix,
                  "time_spent": time_spent
                }
            })
            log.debug('Time spent on %s was %f.', type(method).__name__, time_spent)
        return similarities
