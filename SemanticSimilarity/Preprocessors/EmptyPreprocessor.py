from rdflib import Graph
from SemanticSimilarity.Preprocessors.IPreprocessor import IPreprocessor


class EmptyPreprocessor(IPreprocessor):
    def preprocess(self, graphs: "list[Graph]", files: "list[str]"):
        return graphs, files
