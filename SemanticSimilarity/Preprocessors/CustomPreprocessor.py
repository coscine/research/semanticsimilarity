import numpy as np
from rdflib import ConjunctiveGraph, Graph
from SemanticSimilarity.Preprocessors.IPreprocessor import IPreprocessor
from SemanticSimilarity.Similarities.Custom.oldfilterGraph import (
    createSimilarityTriples,
)


class CustomPreprocessor(IPreprocessor):
    def preprocess(self, graphs: "list[Graph]", files: "list[str]"):
        newGraphs = []
        parseFormat = "trig"
        for graph in graphs:
            serializedGraph = graph.serialize(format=parseFormat, encoding="utf-8")
            newGraph = ConjunctiveGraph()
            newGraph.parse(data=serializedGraph, format=parseFormat)
            createSimilarityTriples(newGraph)
            newGraphs.append(newGraph)
        return newGraphs, files
