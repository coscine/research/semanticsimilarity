from abc import ABCMeta, abstractmethod
from rdflib import Graph

import logging

log = logging.getLogger(__name__)


class IPreprocessor:
    __metaclass__ = ABCMeta

    @abstractmethod
    def preprocess(self, graphs: "list[Graph]", files: "list[str]"):
        raise NotImplementedError
